﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Collider2D))]
public class Asteroid : MonoBehaviour, IEntity {
    #region Public Functions
    public Asteroid Setup (Vector3 position, Vector3 rotation, Vector3 scale, int stage, Action<Transform, int, IEntity> onCollideCallback) {
        transform.position = position;
        transform.eulerAngles = rotation;
        transform.localScale = scale;
        _stage = stage;
        _onCollisionCallback = onCollideCallback;
        return this;
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        _movement = GetComponent<Movement>();
        
        if (TryGetComponent<CircleCollider2D>(out var collider))
            _extraBuffer = collider.radius;
    }

    void OnEnable () {
         var data = GameManager.Instance.GameData.AsteroidData;
        _movement.Setup(Random.Range(data.SpeedRanges[_stage].x, data.SpeedRanges[_stage].y), _extraBuffer);
    }

    void OnTriggerEnter2D (Collider2D other) {
        if (_onCollisionCallback != null) {
            _onCollisionCallback(transform, _stage, other.GetComponent<IEntity>());
            _onCollisionCallback = null;
        }
        PoolManager.Instance.ReturnAsteroid(gameObject);
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    #endregion

    #region Fields, Accessors
    public EntityType Type {
        get;
        set;
    }

    Movement _movement;
    Action<Transform, int, IEntity> _onCollisionCallback;

    float _extraBuffer;

    int _stage;
    #endregion
}