﻿using UnityEngine;

public class Shooting : MonoBehaviour {
    #region Public Functions
    public void Setup (float cooldown, int maxBullets) {
        _firingCooldown = cooldown;
        _maxBullets = maxBullets;
    }

    public void Shoot (bool isPlayer) {
        if (_bulletCount < _maxBullets && _cooldown <= 0f) {
            var bullet = PoolManager.Instance.GetBullet();
            bullet.transform.position = transform.position + transform.up * 0.2f;
            bullet.transform.eulerAngles = transform.eulerAngles;
            _cooldown = _firingCooldown;
            if (bullet.TryGetComponent<Bullet>(out var comp))
                comp.Setup(OnBulletToBeRemoved)
                    .Type = isPlayer ? EntityType.Player : EntityType.Enemy;
            bullet.SetActive(true);
            _bulletCount++;

            AudioManager.Instance.RequestSfx(GameManager.Instance.GameConfig.BulletSfx);
        }
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void OnEnable () {
        _cooldown = _firingCooldown;
        _bulletCount = 0;
    }

    void FixedUpdate () {
        if (_cooldown > 0f)
            _cooldown -= Time.fixedDeltaTime;
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    void OnBulletToBeRemoved () {
        _bulletCount--;
    }
    #endregion

    #region Fields, Accessors
    float _cooldown;
    float _firingCooldown;
    int _maxBullets;
    int _bulletCount;
    #endregion
}