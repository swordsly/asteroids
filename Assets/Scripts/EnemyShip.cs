﻿using System.Collections;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(ShipMovement))]
[RequireComponent(typeof(Shooting))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class EnemyShip : MonoBehaviour, IEntity {
    public enum EnemyState {
        Idle = 0,
        Spawn,
        Move,
        Shoot
    }

    public enum EnemyAction {
        Move_StayOnCourse = 0,
        Move_Up,
        Move_Down,
        Shoot_Random,
        Shoot_Player
    }

    #region Public Functions
    public EnemyShip Setup (Action<IEntity> onCollideCallback) {
        _onCollisionCallback = onCollideCallback;
        return this;
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        _movement = GetComponent<ShipMovement>();
        _shooting = GetComponent<Shooting>();
        if(TryGetComponent<CircleCollider2D>(out var collider)) {
            _extraBuffer = collider.radius;
            _collider = collider;
        }

        var data = GameManager.Instance.GameData.EnemyData;
        _movement.Setup(data.ThrustRate, _extraBuffer, data.DecelerationRate, data.MaxThrust, data.RotationRate);
        _shooting.Setup(data.FiringCooldown, data.MaxBullet);
    }

    void OnEnable () {
        _collider.enabled = false;
        StartCoroutine(DecisionRoutine());
        _currentAction = EnemyAction.Move_StayOnCourse;
        _currentState = EnemyState.Spawn;
    }

    void OnDisable () {
        StopCoroutine(DecisionRoutine());
        _currentState = EnemyState.Idle;
        transform.position = new Vector3(GameManager.FieldBounds.x + _extraBuffer * 2f, 0f, 0f);
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            _currentState = EnemyState.Idle;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            _currentState = EnemyState.Spawn;
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            _currentState = EnemyState.Move;
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            _currentState = EnemyState.Shoot;
        
        if (Input.GetKeyDown(KeyCode.Q)) {
            _currentState = EnemyState.Move;
            _currentAction = EnemyAction.Move_StayOnCourse;
        }
        else if (Input.GetKeyDown(KeyCode.W)) {
            _currentState = EnemyState.Move;
            _currentAction = EnemyAction.Move_Up;
        }
        else if (Input.GetKeyDown(KeyCode.E)) {
            _currentState = EnemyState.Move;
            _currentAction = EnemyAction.Move_Down;
        }
        else if (Input.GetKeyDown(KeyCode.A)) { 
            _currentState = EnemyState.Shoot;
            _currentAction = EnemyAction.Shoot_Random;
        }
        else if (Input.GetKeyDown(KeyCode.S)) {
            _currentState = EnemyState.Shoot;
            _currentAction = EnemyAction.Shoot_Player;
        }
        
    }

    void OnTriggerEnter2D (Collider2D other) {
        if (other.TryGetComponent<IEntity>(out var entity)) {
            if (entity.Type == Type)
                return;
            
            if (_onCollisionCallback != null)
                _onCollisionCallback(entity);
            gameObject.SetActive(false);
        }
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    float NextMove () {
        float wait = 0f;
        int moveOrShoot = Random.Range(0, 2);
        if (moveOrShoot == 0) {
            int action = Random.Range(0, 3);
            if (action == 0) {
                wait = 0.5f;
                _currentAction = EnemyAction.Move_Up;
            } else if (action == 1){
                wait = 0.5f;
                _currentAction = EnemyAction.Move_Down;
            } else{
                wait = 1f;
                _currentAction = EnemyAction.Move_StayOnCourse;
            }
            _currentState = EnemyState.Move;
        } else {
            int action = Random.Range(0, 2);
            if (action == 0) {
                wait = 0.5f;
                _currentAction = EnemyAction.Shoot_Random;
            } else {
                wait = 0.5f;
                _currentAction = EnemyAction.Shoot_Player;
            }
            _currentState = EnemyState.Shoot;
        }
        return wait;
    }
    
    IEnumerator DecisionRoutine () {
        float wait = 0f;
        while (isActiveAndEnabled) {
            switch (_currentState) {
                case EnemyState.Spawn:
                    yield return DoSpawn();
                    _currentState = EnemyState.Move;
                    _currentAction = EnemyAction.Move_StayOnCourse;
                    _collider.enabled = true;
                    wait = 0.5f;
                    break;
                case EnemyState.Move:
                    yield return DoMove();
                    wait = NextMove();
                    break;
                case EnemyState.Shoot:
                    yield return DoShoot();
                    wait = NextMove();
                    break;
            }

            yield return new WaitForSeconds(wait);
        }
    }

    IEnumerator DoSpawn () {
        int c1 = Random.Range(0, 2);
        float spawnPoint = GameManager.FieldBounds.x + _extraBuffer * 2f;

        transform.eulerAngles = Vector3.zero;
        if (c1 > 0) {
            transform.position = new Vector3(spawnPoint, 0f, 0f);
            _movement.RotatingState = ShipMovement.RotateState.Left;
        } else {
            transform.position = new Vector3(-spawnPoint, 0f, 0f);
            _movement.RotatingState = ShipMovement.RotateState.Right;
        }

        int count = (int) (90f / GameManager.Instance.GameData.EnemyData.RotationRate);
        while (count > 0) {
            yield return new WaitForFixedUpdate();
            count--;
        }

        _movement.RotatingState = ShipMovement.RotateState.Idle;
        _movement.NewHeading = transform.up;
    }

    IEnumerator DoMove () {
        _movement.MovingState = ShipMovement.MoveState.Thrust;
        switch (_currentAction) {
            case EnemyAction.Move_Up:
                _movement.RotatingState = (transform.up.x > 0f) ? ShipMovement.RotateState.Left : ShipMovement.RotateState.Right;
                int count = (int) (45f / GameManager.Instance.GameData.EnemyData.RotationRate);
                while (count > 0) {
                    yield return new WaitForFixedUpdate();
                    count--;
                }
                break;
            case EnemyAction.Move_Down:
                _movement.RotatingState = (transform.up.x > 0f) ? ShipMovement.RotateState.Right : ShipMovement.RotateState.Left;
                count = (int) (45f / GameManager.Instance.GameData.EnemyData.RotationRate);
                while (count > 0) {
                    yield return new WaitForFixedUpdate();
                    count--;
                }
                break;
        }
        _movement.RotatingState = ShipMovement.RotateState.Idle;
        _movement.NewHeading = transform.up;
    }

    IEnumerator DoShoot () {
        switch (_currentAction) {
            case EnemyAction.Shoot_Random:
                var rotate = (Random.Range(0, 2) > 0f) ? ShipMovement.RotateState.Left : ShipMovement.RotateState.Right;
                _movement.RotatingState = rotate;
                int count = Random.Range(0, 24);
                var temp = count;
                while (temp > 0) {
                    yield return new WaitForFixedUpdate();
                    temp--;
                }
                _movement.RotatingState = ShipMovement.RotateState.Idle;
                yield return new WaitForSeconds(0.2f);
                _shooting.Shoot(false);
                yield return new WaitForSeconds(0.2f);
                _movement.RotatingState = (rotate == ShipMovement.RotateState.Right) ? ShipMovement.RotateState.Left : ShipMovement.RotateState.Right;
                temp = count;
                while (temp > 0) {
                    yield return new WaitForFixedUpdate();
                    temp--;
                }
                _movement.RotatingState = ShipMovement.RotateState.Idle;
                _currentAction = EnemyAction.Move_StayOnCourse;
                break;
            case EnemyAction.Shoot_Player:
                var dir = GameManager.Instance.PlayerShip.transform.position - transform.position;
                dir = GameManager.Instance.PlayerShip.transform.InverseTransformDirection(dir);
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                if (transform.up.x < 0f) {
                    rotate = (angle < 0f) ? ShipMovement.RotateState.Left : ShipMovement.RotateState.Right;
                    angle = (rotate == ShipMovement.RotateState.Left) ? angle + 180f : angle - 180f;
                } else {
                    rotate = (angle > 0f) ? ShipMovement.RotateState.Left : ShipMovement.RotateState.Right;
                }
                _movement.RotatingState = rotate;
                temp = count = (int) Mathf.Abs((angle / GameManager.Instance.GameData.EnemyData.RotationRate));
                while (temp > 0) {
                    yield return new WaitForFixedUpdate();
                    temp--;
                }
                _movement.RotatingState = ShipMovement.RotateState.Idle;
                yield return new WaitForSeconds(0.2f);
                _shooting.Shoot(false);
                yield return new WaitForSeconds(0.2f);
                _movement.RotatingState = (rotate == ShipMovement.RotateState.Right) ? ShipMovement.RotateState.Left : ShipMovement.RotateState.Right;
                temp = count;
                while (temp > 0) {
                    yield return new WaitForFixedUpdate();
                    temp--;
                }
                _movement.RotatingState = ShipMovement.RotateState.Idle;
                _currentAction = EnemyAction.Move_StayOnCourse;
                break;
        }
    }        
    #endregion

    #region Fields, Accessors
    public EntityType Type {
        get;
        set;
    }

    ShipMovement _movement;
    Shooting _shooting;
    
    bool _isAlive;

    float _extraBuffer;

    EnemyState _currentState = EnemyState.Idle;
    EnemyAction _currentAction = EnemyAction.Move_StayOnCourse;

    Action<IEntity> _onCollisionCallback;

    Collider2D _collider;
    #endregion
}