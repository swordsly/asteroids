﻿using System;
using UnityEngine;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Collider2D))]
public class Bullet : MonoBehaviour, IEntity {
    #region Public Functions
    public Bullet Setup (Action onCollideCallback) {
        _onCollisionCallback = onCollideCallback;
        return this;
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        _movement = GetComponent<Movement>();

        if (TryGetComponent<CircleCollider2D>(out var collider))
            _extraBuffer = collider.radius;
        
        _movement.Setup(GameManager.Instance.GameData.BulletData.Speed, _extraBuffer);
    }

    void OnEnable () {
        _timeLeft = GameManager.Instance.GameData.BulletData.TimeToLive;
    }

    void FixedUpdate () {
        _timeLeft -= Time.fixedDeltaTime;
        if (_timeLeft <= 0f)
            RemoveBullet();
    }

    void OnTriggerEnter2D (Collider2D other) {
        if (other.TryGetComponent<IEntity>(out var entity) && entity.Type == Type)
            return;
        RemoveBullet();
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    void RemoveBullet () {
        if (_onCollisionCallback != null) {
            _onCollisionCallback();
            _onCollisionCallback = null;
        }
        PoolManager.Instance.ReturnBullet(gameObject);
    }
    #endregion

    #region Fields, Accessors
    public EntityType Type {
        get;
        set;
    }

    Movement _movement;
    Action _onCollisionCallback;

    float _extraBuffer;

    float _timeLeft;
    #endregion
}