﻿using UnityEngine;

public class Movement : MonoBehaviour {
    public enum Exit {
        Entered = 0,
        Exited,
        Exiting
    }

    #region Public Functions
    public virtual void Setup (float speed, float radius) {
        _speed = speed;
        _radius = radius;
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void OnEnable () {
        _heading = transform.up;
    }

    void FixedUpdate () {
        Move(_speed);
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    protected void Move (float speed) {
        //  if Exiting, wrap -> Exited
        if (_exit == Exit.Exiting) {
            transform.position = WrapAroundField(transform.position, out var hasWarped);
            _exit = Exit.Exited;
            return;
        }

        var nextPosition = transform.position + _heading * speed * Time.fixedDeltaTime;
        WrapAroundField(nextPosition, out var isOutside);
        //  if next position is outside && Exited -> just move
        transform.position = nextPosition;
        if (_exit == Exit.Exited) {
            //  if next position is inside && Exited -> Entered
            if (!isOutside)
                _exit = Exit.Entered;
            return;
        }
            
        //  if next position is outside && Entered -> Exiting
        if (isOutside)
            _exit = Exit.Exiting;
    }

    protected Vector3 WrapAroundField (Vector3 position, out bool hasWrapped) {
        hasWrapped = false;
        Vector3 result = position;

        // y <--> -y
        if (Mathf.Abs(0f - position.y) > GameManager.FieldBounds.y + _radius) {
            result.y = -result.y;
            hasWrapped = true;
        }
        // x <--> -x
        if (Mathf.Abs(0f - position.x) > GameManager.FieldBounds.x + _radius) {
            result.x = -result.x;
            hasWrapped = true;
        }
        
        return result;
    }
    #endregion

    #region Fields, Accessors
    protected Vector3 _heading;
    protected float _speed;
    protected float _radius;
    
    protected Exit _exit;
    #endregion
}