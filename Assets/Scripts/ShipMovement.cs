﻿using UnityEngine;

public class ShipMovement : Movement {
    public enum MoveState {
        Idle = 0,
        Thrust,
        Decelerate
    }

    public enum RotateState {
        Idle = 0,
        Left,
        Right
    }

    #region Public Functions
    public void Setup (float speed, float radius, float decelerate, float maxSpeed, float rotationRate) {
        base.Setup(speed, radius);
        _decelerate = decelerate;
        _maxSpeed = maxSpeed;
        _rotateBy = rotationRate;
    }

    public void DoHyperSpace () {
        if (!IsPlayer)
            return;
        
        float smallerWidth = GameManager.FieldBounds.x - _radius * 2;
        float smallerHeight = GameManager.FieldBounds.y - _radius * 2;
        transform.position = new Vector3(Random.Range(-smallerWidth, smallerWidth), Random.Range(-smallerHeight, smallerHeight), 0f);
        _thrust = 0f;
        MovingState = MoveState.Idle;
    }
    #endregion

    #region Inspector Variables
    [SerializeField] bool IsPlayer;
    #endregion

    #region MonoBehaviour Functions
    void OnEnable () {
        _heading = NewHeading = transform.up;
        _thrust = 0f;
        _movingState = MoveState.Idle;
        _rotatingState = RotateState.Idle;
        _exit = IsPlayer ? Exit.Entered : Exit.Exited;
    }

    void FixedUpdate () {
        if (!enabled) return;

        if (_rotatingState != RotateState.Idle)
            transform.eulerAngles += new Vector3(0f, 0f, _rotateBy);

        switch (_movingState) {
            case MoveState.Thrust:
                _thrust = Mathf.Min(_maxSpeed, _thrust + _speed);
                Move(_thrust);
                break;
            case MoveState.Decelerate:
                _thrust = Mathf.Max(0f, _thrust - _decelerate);
                Move(_thrust);
                if (_thrust <= 0f)
                    MovingState = MoveState.Idle;
                break;
        }
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    #endregion

    #region Fields, Accessors
    [HideInInspector]
    public Vector3 NewHeading;
    public MoveState MovingState {
        set {
            _movingState = value;
            if (value == MoveState.Thrust)
                _heading = NewHeading;
        }
    }
    public RotateState RotatingState {
        set {
            _rotatingState = value;
            if (value == RotateState.Left)
                _rotateBy = Mathf.Abs(_rotateBy);
            else if (value == RotateState.Right)
                _rotateBy = Mathf.Abs(_rotateBy) * -1f;
        }
    }

    MoveState _movingState = MoveState.Idle;
    RotateState _rotatingState = RotateState.Idle;

    float _decelerate;
    float _maxSpeed;
    float _thrust;
    float _rotateBy;
    #endregion
}