﻿using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour {
    #region Public Functions
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        Instance = this as T;
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    #endregion

    #region Fields, Accessors
    static T instance;
    public static T Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<T>();
                if (instance == null) {
                    GameObject _singleton = new GameObject();
                    Instance = _singleton.AddComponent<T>();
                }
            }
            return instance;
        }
        private set {
            if (instance != null) return;

            instance = value;
            instance.gameObject.name = "(Singleton)-" + typeof(T).Name;
            DontDestroyOnLoad(instance.gameObject);
        }
    }
    #endregion
}