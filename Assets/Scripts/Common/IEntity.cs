﻿public interface IEntity {
    EntityType Type {
        get;
        set;
    }
}

public enum EntityType {
    Player = 0,
    Asteroid,
    Enemy
}