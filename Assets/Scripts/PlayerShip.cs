﻿using UnityEngine;
using System;

[RequireComponent(typeof(ShipMovement))]
[RequireComponent(typeof(Shooting))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class PlayerShip : MonoBehaviour, IEntity {
    #region Public Functions
    public PlayerShip Setup (Action onCollideCallback) {
        _onCollisionCallback = onCollideCallback;
        return this;
    }
    #endregion

    #region Inspector Variables

    [Header("References")]
    [SerializeField] SpriteRenderer jetFire;
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        _movement = GetComponent<ShipMovement>();
        _shooting = GetComponent<Shooting>();
        if(TryGetComponent<CircleCollider2D>(out var collider))
            _extraBuffer = collider.radius;

        if (jetFire != null)
            jetFire.enabled = false;

        var data = GameManager.Instance.GameData.PlayerData;
        _movement.Setup(data.ThrustRate, _extraBuffer, data.DecelerationRate, data.MaxThrust, data.RotationRate);
        _shooting.Setup(data.FiringCooldown, data.MaxBullet);
    }
    
    void OnEnable () {
        transform.position = transform.eulerAngles = Vector3.zero;
        jetFire.enabled = false;
    }

    void Update () {
        if (Input.GetKeyDown(HYPERSPACE))
            _movement.DoHyperSpace();

        if (Input.GetKeyDown(ROTATE_LEFT))
            _movement.RotatingState = ShipMovement.RotateState.Left;
        else if (Input.GetKeyDown(ROTATE_RIGHT))
            _movement.RotatingState = ShipMovement.RotateState.Right;
        else if (Input.GetKey(ROTATE_LEFT) || Input.GetKey(ROTATE_RIGHT))
            _movement.NewHeading = transform.up;
        else if (Input.GetKeyUp(ROTATE_LEFT) || Input.GetKeyUp(ROTATE_RIGHT))
            _movement.RotatingState = ShipMovement.RotateState.Idle;
        
        if (Input.GetKeyDown(THRUST)) {
            _movement.MovingState = ShipMovement.MoveState.Thrust;
            jetFire.enabled = true;
        } else if (Input.GetKeyUp(THRUST)) {
            _movement.MovingState = ShipMovement.MoveState.Decelerate;
            jetFire.enabled = false;
        }
        
        if (Input.GetKeyDown(FIRE))
            _shooting.Shoot(true);    
    }

    void OnTriggerEnter2D (Collider2D other) {
        if (other.TryGetComponent<IEntity>(out var entity)) {
            if (entity.Type == EntityType.Player)
                return;
            
            if (_onCollisionCallback != null)
                _onCollisionCallback();
            gameObject.SetActive(false);
        }        
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    #endregion

    #region Fields, Accessors
    public EntityType Type {
        get;
        set;
    }
    
    float _extraBuffer;

    ShipMovement _movement;
    Shooting _shooting;

    Action _onCollisionCallback;

    static KeyCode ROTATE_LEFT = KeyCode.LeftArrow;
    static KeyCode ROTATE_RIGHT = KeyCode.RightArrow;
    static KeyCode THRUST = KeyCode.UpArrow;
    static KeyCode HYPERSPACE = KeyCode.DownArrow;
    static KeyCode FIRE = KeyCode.Space;
    #endregion
}