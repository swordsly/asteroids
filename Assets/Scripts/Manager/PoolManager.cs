﻿using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoSingleton<PoolManager> {
    #region Public Functions
    public GameObject GetBullet () {
        if (_bulletStack.Count > 0)
            return _bulletStack.Pop();
        
        return Instantiate<GameObject>(GameManager.Instance.GameConfig.BulletPoolConfig.Prefab, 
                                       Vector3.zero, 
                                       Quaternion.identity, 
                                       _bulletContainer);
    }

    public void ReturnBullet (GameObject bullet) {
        bullet.SetActive(false);
        _bulletStack.Push(bullet);
    }

    public GameObject GetAsteroid () {
        if (_asteroidStack.Count > 0)
            return _asteroidStack.Pop();
        
        return Instantiate<GameObject>(GameManager.Instance.GameConfig.AsteroidPoolConfig.Prefab,
                                       Vector3.zero, 
                                       Quaternion.identity, 
                                       _asteroidContainer);
    }

    public void ReturnAsteroid (GameObject asteroid) {
        asteroid.SetActive(false);
        _asteroidStack.Push(asteroid);
    }

    public void CleanUp () {
        _bulletStack.Clear();
        foreach (Transform child in _bulletContainer.transform) {
            child.gameObject.SetActive(false);
            _bulletStack.Push(child.gameObject);
        }
        _asteroidStack.Clear();
        foreach (Transform child in _asteroidContainer) {
            child.gameObject.SetActive(false);
            _asteroidStack.Push(child.gameObject);
        }
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        PrepareAssets();
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    void PrepareAssets () {
        _bulletContainer = GameObject.Find("BulletContainer")?.transform;
        _bulletStack = new Stack<GameObject>(GameManager.Instance.GameConfig.BulletPoolConfig.PoolSize);
        for (int i = 0; i < GameManager.Instance.GameConfig.BulletPoolConfig.PoolSize; i++) {
            var bullet = Instantiate<GameObject>(GameManager.Instance.GameConfig.BulletPoolConfig.Prefab,
                                                 Vector3.zero, 
                                                 Quaternion.identity, 
                                                 _bulletContainer);
            _bulletStack.Push(bullet);
        }

        _asteroidContainer = GameObject.Find("AsteroidContainer")?.transform;
        _asteroidStack = new Stack<GameObject>(GameManager.Instance.GameConfig.AsteroidPoolConfig.PoolSize);
        for (int i = 0; i < GameManager.Instance.GameConfig.AsteroidPoolConfig.PoolSize; i++) {
            var asteroid = Instantiate<GameObject>(GameManager.Instance.GameConfig.AsteroidPoolConfig.Prefab, 
                                                   Vector3.zero, 
                                                   Quaternion.identity, 
                                                   _asteroidContainer);
            _asteroidStack.Push(asteroid);
        }
    }
    #endregion

    #region Fields, Accessors
    Stack<GameObject> _bulletStack;
    Stack<GameObject> _asteroidStack;

    Transform _bulletContainer;
    Transform _asteroidContainer;
    #endregion
}