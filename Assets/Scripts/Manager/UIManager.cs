﻿using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class UIManager : MonoSingleton<UIManager> {
    #region Public Functions
    #endregion

    #region Inspector Variables
    [SerializeField] CanvasGroup menuCanvasGrp;
    [SerializeField] Button playBtn;

    [Space]

    [SerializeField] CanvasGroup playCanvasGrp;
    [SerializeField] TMP_Text scoreTxt;
    [SerializeField] TMP_Text hiScoreTxt;
    [SerializeField] TMP_Text lifeTxt;

    [Space]

    [SerializeField] CanvasGroup gameOverCanvasGrp;

    [Space]

    [SerializeField] Toggle muteToggle;
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        playBtn.onClick.AddListener(PlayGame);
        muteToggle.onValueChanged.AddListener(SetMute);
    }

    void Start () {
        GameManager.Instance.OnPlayerLifeUpdate += (int life) => { lifeTxt.text = $"Life: {life}"; };
        GameManager.Instance.OnScoreUpdate += (int score) => { scoreTxt.text = $"{score}"; };
        GameManager.Instance.OnHiScoreUpdate += (int hiScore) => { hiScoreTxt.text = $"{hiScore}"; };
        GameManager.Instance.OnGameEnd += EndGame;
        GameManager.Instance.OnGoToMenu += GoToMenu;
    }
    #endregion

    #region Delegates, Events
    public event Action OnPlayBtnClicked;
    public event Action<bool> OnMuteBtnClicked;
    #endregion

    #region Private Functions
    void PlayGame () {
        menuCanvasGrp.Hide();
        playCanvasGrp.Show();
        if (OnPlayBtnClicked != null)
            OnPlayBtnClicked();
    }

    void EndGame () {
        playCanvasGrp.Hide();
        gameOverCanvasGrp.Show();
    }

    void GoToMenu () {
        gameOverCanvasGrp.Hide();
        menuCanvasGrp.Show();
    }

    void SetMute (bool isMute) {
        if (OnMuteBtnClicked != null)
            OnMuteBtnClicked(isMute);
    }
    #endregion

    #region Fields, Accessors
    #endregion
}