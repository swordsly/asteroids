﻿using System.Collections;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoSingleton<GameManager> {
    public enum GameState {
        None = 0,
        Menu,
        Play,
        End
    }

    #region Public Functions
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        _gameData = Resources.Load<GameData>(GAME_DATA_PATH);
        _gameConfig = Resources.Load<GameConfiguration>(GAME_CONFIG_PATH);

        var cam = Camera.main;
        var bounds = cam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, cam.transform.position.z));
        _fieldBounds = new Vector2(bounds.x, bounds.y);
        
        if (_player == null)
            (_player = Instantiate<GameObject>(_gameConfig.PlayerShipPrefab, Vector3.zero, Quaternion.identity).GetComponent<PlayerShip>())
                                                                                                               .Setup(PlayerGetsHit)
                                                                                                               .Type = EntityType.Player;
        if (_enemy == null)
            (_enemy = Instantiate<GameObject>(_gameConfig.EnemyShipPrefab, new Vector3(_fieldBounds.x + 1.5f, 0f, 0f), Quaternion.identity).GetComponent<EnemyShip>())
                                                                                                                                           .Setup(EnemyGetsHit)
                                                                                                                                           .Type = EntityType.Enemy;
        var pm = PoolManager.Instance;
    }

    void Start () {
        AudioManager.Instance.RequestLongPlay(_gameConfig.Bgm, true);

        UIManager.Instance.OnPlayBtnClicked += () => { _currentState = GameState.Play; };

        StartCoroutine(MainRoutine());
    }
    #endregion

    #region Delegates, Events
    public event Action<int> OnPlayerLifeUpdate;
    public event Action<int> OnScoreUpdate;
    public event Action<int> OnHiScoreUpdate;
    public event Action OnGameEnd;
    public event Action OnGoToMenu;
    #endregion

    #region Private Functions
    void SpawnAsteroid (Vector3 position, Vector3 rotation, int stage) {
        var asteroid = PoolManager.Instance.GetAsteroid();
        if (asteroid.TryGetComponent<Asteroid>(out var comp))
            comp.Setup(position, rotation, Vector3.one * GameManager.Instance.GameData.AsteroidData.Sizes[stage], stage, Break)
                .Type = EntityType.Asteroid;
        asteroid.SetActive(true);
    }

    void InitAsteroids () {
         _asteroidCount = Random.Range(_gameData.AsteroidSpawnRange.x, _gameData.AsteroidSpawnRange.y);
        for (int i = 0; i < _asteroidCount; i++) {
            Vector3 position = new Vector3(Random.Range(-1f, 1f) * GameManager.FieldBounds.x,
                                           Random.Range(-1f, 1f) * GameManager.FieldBounds.y,
                                           0f);
            Vector3 rotation = new Vector3(0f, 0f, Random.Range(0f, 359f));
            SpawnAsteroid(position, rotation, 0);
        }
    }

    void Break (Transform originalTransform, int stage, IEntity entity) {
        _asteroidCount--;
        AudioManager.Instance.RequestSfx(_gameConfig.BreakSfx);
        
        if (entity.Type == EntityType.Player) {
            ScoreIncrement += _gameData.AsteroidData.Points[stage];
            Score += _gameData.AsteroidData.Points[stage];
        }

        if (stage == 2) {
            if (_asteroidCount > 0)
                return;
            
            InitAsteroids();
            return;
        }

        SpawnAsteroid(originalTransform.position, 
                      new Vector3(0f, 0f, originalTransform.eulerAngles.z - 45f),
                      ++stage);
        SpawnAsteroid(originalTransform.position, 
                      new Vector3(0f, 0f, originalTransform.eulerAngles.z + 45f),
                      stage);

        _asteroidCount += 2;
    }

    void PlayerGetsHit () {
        if (_currentState != GameState.Play)
            return;

        PlayerLife--;
        AudioManager.Instance.RequestSfx(_gameConfig.HitSfx);
        if (_playerLife <= 0) {
            PlayerLife = 0;
            if (OnGameEnd != null)
                OnGameEnd();
            _currentState = GameState.End;
            return;
        }

        StopCoroutine(ReactivatePlayer());
        StartCoroutine(ReactivatePlayer());
    }

    void EnemyGetsHit (IEntity entity) {
        if (entity.Type == EntityType.Player) {
            ScoreIncrement += _gameData.EnemyPoints;
            Score += _gameData.EnemyPoints;
        }
        AudioManager.Instance.RequestSfx(_gameConfig.HitSfx);
    }

    IEnumerator MainRoutine () {
        GameState previousState = GameState.None;
        while (isActiveAndEnabled) {
            yield return new WaitUntil(() => previousState != _currentState);
            previousState = _currentState;

            switch (_currentState) {
                case GameState.Menu:
                    if (_score > _hiScore) {
                        HiScore = _score;
                        PlayerPrefs.SetInt("hiscore", _hiScore);
                    }

                    Score = 0;
                    if (PlayerPrefs.HasKey("hiscore"))
                        HiScore = PlayerPrefs.GetInt("hiscore");
                    else
                        HiScore = 0;
                    break;
                case GameState.Play:
                    if (_hasInit)
                        PoolManager.Instance.CleanUp();

                    _enemy.gameObject.SetActive(false);
                    StopCoroutine(ActivateEnemy());
                    StartCoroutine(ActivateEnemy());
                    
                    InitAsteroids();
                    _hasInit = true;

                    PlayerLife = _gameData.PlayerStartLife;
                    _player.gameObject.SetActive(true);
                    break;
                case GameState.End:
                    yield return new WaitForSeconds(2f);
                    if (OnGoToMenu != null)
                        OnGoToMenu();
                    _currentState = GameState.Menu;
                    break;
            }
        }
    }

    IEnumerator ReactivatePlayer () {
        yield return new WaitForSeconds(_gameData.PlayerRespawnInterval);
        _player.transform.position = Vector3.zero;
        _player.transform.eulerAngles = Vector3.zero;
        _player.gameObject.SetActive(true);
    }

    IEnumerator ActivateEnemy () {
        while (isActiveAndEnabled) {
            yield return new WaitUntil(() => (_score >= _gameData.PointsToSpawnEnemy));
            _enemy.gameObject.SetActive(true);
            AudioManager.Instance.RequestLongPlay(_gameConfig.EnemySfx, true);
            yield return new WaitUntil(() => (!_enemy.gameObject.activeInHierarchy));
            AudioManager.Instance.RequestStopLongPlay(_gameConfig.EnemySfx);
            yield return new WaitForSeconds(_gameData.EnemyRespawnInterval);
        }
    }
    #endregion

    #region Fields, Accessors
    public static Vector2 FieldBounds => _fieldBounds;

    public GameData GameData => _gameData;
    public GameConfiguration GameConfig => _gameConfig;
    public PlayerShip PlayerShip => _player;

    static Vector2 _fieldBounds;
    
    static readonly string GAME_DATA_PATH = "Data/game_data";
    static readonly string GAME_CONFIG_PATH = "Data/game_config";

    GameData _gameData;
    GameConfiguration _gameConfig;
    PlayerShip _player;
    EnemyShip _enemy;

    GameState _currentState = GameState.Menu;

    int _playerLife;
    int PlayerLife {
        get {
            return _playerLife;
        }
        set {
            _playerLife = value;
            if (OnPlayerLifeUpdate != null)
                OnPlayerLifeUpdate(_playerLife);
        }
    }
    int _score;
    int Score {
        get {
            return _score;
        }
        set {
            _score = value;
            if (OnScoreUpdate != null)
                OnScoreUpdate(_score);
        }
    }
    int _hiScore;
    int HiScore {
        get {
            return _hiScore;
        }
        set {
            _hiScore = value;
            if (OnHiScoreUpdate != null)
                OnHiScoreUpdate(_hiScore);
        }
    }
    int _scoreIncrement;
    int ScoreIncrement {
        get {
            return _scoreIncrement;
        }
        set {
            if (value >= _gameData.PointsToGainLife) {
                PlayerLife = Mathf.Min(_gameData.PlayerMaxLife, _playerLife + 1);
                value = 0;
            }
            _scoreIncrement = value;
        }
    }

    int _asteroidCount;

    bool _hasInit = false;
    #endregion
}