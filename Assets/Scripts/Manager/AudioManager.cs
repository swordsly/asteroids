﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoSingleton<AudioManager> {
    #region Public Functions
    public void RequestSfx (AudioClip clip) {
        _sfxChannel.PlayOneShot(clip);
    }

    public void RequestLongPlay (AudioClip clip, bool isLooping) {
        foreach (var channel in _bgmChannels) {
            if (channel.isPlaying)
                continue;
            
            if (!_blockedBgmChannels.ContainsKey(clip)) {
                channel.clip = clip;
                channel.loop = isLooping;
                channel.Play();
                _blockedBgmChannels.Add(clip, channel);
                if (!isLooping)
                    StartCoroutine(ReleaseChannelRoutine(clip));
                break;
            }
        }
    }

    public void RequestStopLongPlay (AudioClip clip) {
        if (_blockedBgmChannels.ContainsKey(clip)) {
            _blockedBgmChannels[clip].Stop();
            _blockedBgmChannels.Remove(clip);
        }
    }
    #endregion

    #region Inspector Variables
    #endregion

    #region MonoBehaviour Functions
    void Awake () {
        (_sfxChannel = gameObject.AddComponent<AudioSource>())
                                 .playOnAwake = false;
        _bgmChannels = new AudioSource[GameManager.Instance.GameConfig.BgmChannelCount];
        for (int i = 0; i < GameManager.Instance.GameConfig.BgmChannelCount; i++)
            (_bgmChannels[i] = gameObject.AddComponent<AudioSource>())
                                         .playOnAwake = false;
    }

    void Start() {
        UIManager.Instance.OnMuteBtnClicked += SetMute;
    }
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    void SetMute (bool isMute) {
        _sfxChannel.mute = isMute;
        foreach (var channel in _bgmChannels)
            channel.mute = isMute;
    }

    IEnumerator ReleaseChannelRoutine (AudioClip clip) {
        yield return new WaitForSeconds(clip.length);
        _blockedBgmChannels[clip].Stop();
        _blockedBgmChannels.Remove(clip);
    }
    #endregion

    #region Fields, Accessors
    AudioSource _sfxChannel;
    AudioSource[] _bgmChannels;
    Dictionary<AudioClip, AudioSource> _blockedBgmChannels = new Dictionary<AudioClip, AudioSource>();
    #endregion
}