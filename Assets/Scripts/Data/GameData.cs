﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/Game Data", fileName = "game_data")]
public class GameData : ScriptableObject {
    #region Public Functions
    #endregion

    #region Inspector Variables
    [Header("Player Data")]
    [SerializeField] PlayerData playerData;
    [SerializeField] int playerStartLife;
    [SerializeField] int playerMaxLife;
    [SerializeField] float playerRespawnInterval;
    [SerializeField] int pointsToGainLife;

    [Space]

    [Header("Bullet Data")]
    [SerializeField] BulletData bulletData;

    [Space]

    [Header("Asteroid Data")]
    [SerializeField] AsteroidData asteroidData;
    [SerializeField] Vector2Int asteroidSpawnRange; 

    [Header("Enemy Data")]
    [SerializeField] PlayerData enemyData;
    [SerializeField] int enemyPoints;
    [SerializeField] int pointsToSpawnEnemy;
    [SerializeField] float enemyRespawnInterval;
    #endregion

    #region MonoBehaviour Functions
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    #endregion

    #region Fields, Accessors
    public PlayerData PlayerData => playerData;
    public int PlayerStartLife => playerStartLife;
    public int PlayerMaxLife => playerMaxLife;
    public float PlayerRespawnInterval => playerRespawnInterval;
    public int PointsToGainLife => pointsToGainLife;

    public BulletData BulletData => bulletData;

    public AsteroidData AsteroidData => asteroidData;
    public Vector2Int AsteroidSpawnRange => asteroidSpawnRange;

    public PlayerData EnemyData => enemyData;
    public int EnemyPoints => enemyPoints;
    public int PointsToSpawnEnemy => pointsToSpawnEnemy;
    public float EnemyRespawnInterval => enemyRespawnInterval;
    #endregion
}

[System.Serializable]
public class PlayerData {
    public float ThrustRate;
    public float DecelerationRate;
    public float MaxThrust;
    public float RotationRate;
    public int MaxBullet;
    public float FiringCooldown;
}

[System.Serializable]
public class BulletData {
    public float Speed;
    public float TimeToLive;
}

[System.Serializable]
public class AsteroidData {
    public Vector2[] SpeedRanges;
    public float[] Sizes;
    public int[] Points;
}