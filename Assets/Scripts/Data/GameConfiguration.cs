﻿using UnityEngine;

[CreateAssetMenu(menuName="Data/Game Config", fileName="game_config")]
public class GameConfiguration : ScriptableObject {
    #region Public Functions
    #endregion

    #region Inspector Variables
    [Header("Bullet")]
    [SerializeField] PoolConfig bulletPoolConfig;
    
    [Header("Asteroid")]
    [SerializeField] PoolConfig asteroidPoolConfig;

    [Header("Prefabs")]
    [SerializeField] GameObject playerShip;
    [SerializeField] GameObject enemyShip;

    [Header("Audio")]
    [SerializeField] int bgmChannelCount;
    [SerializeField] AudioClip bulletSfx;
    [SerializeField] AudioClip hitSfx;
    [SerializeField] AudioClip breakSfx;
    [SerializeField] AudioClip enemySfx;
    [SerializeField] AudioClip bgm;
    #endregion

    #region MonoBehaviour Functions
    #endregion

    #region Delegates, Events
    #endregion

    #region Private Functions
    #endregion
    #region Fields, Accessors

    public PoolConfig BulletPoolConfig => bulletPoolConfig;
    public PoolConfig AsteroidPoolConfig => asteroidPoolConfig;
    public GameObject PlayerShipPrefab => playerShip;
    public GameObject EnemyShipPrefab => enemyShip;

    public int BgmChannelCount => bgmChannelCount;
    public AudioClip BulletSfx => bulletSfx;
    public AudioClip HitSfx => hitSfx;
    public AudioClip BreakSfx => breakSfx;
    public AudioClip EnemySfx => enemySfx;
    public AudioClip Bgm => bgm;
    #endregion
}

[System.Serializable]
public class PoolConfig {
    public GameObject Prefab;
    public int PoolSize;
}